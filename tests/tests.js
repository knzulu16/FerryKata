const assert = require('assert');

var Car = require('../car.js');
var Ferry = require('../ferry.js');
var newBoards = new Ferry(20, 50);

describe('Max cars and Max passengers', function() {
  it("should specify the color of cars and number of passengers in the car", function() {

    var whiteCar = new Car('white', 10);
    assert.equal(whiteCar.color, "white");
    assert.equal(whiteCar.numPass, 10);
  })
  it("should specify the number of cars allowed on the ferry", function() {

    assert.equal(newBoards.maxCars, 20);

  });

  it("The constructor should specify the number of passengers are allowed on the ferry", function() {

    assert.equal(newBoards.maxPeople, 50);

  })
})

describe('Ferry', function() {

  it("should return accepted when the ferry is not full yet", function() {
    console.log(newBoards.onBoard(new Car('black', 2, 'CA 451')));

    results = newBoards.onBoard(new Car('black', 2, 'CA 451'));
    assert.equal(results, "accepted");

  });
  it("Should give free trip after 7 trips on any ferry, the board method should return 'you go free!'", function() {
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));


    console.log(newBoards.onBoard(new Car('black', 2, 'CA 451')));

    assert.deepEqual(results, "accepted");

  });
  it("should return rejected when the ferry is full", function() {
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));

    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));

    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    newBoards.onBoard(new Car('black', 2, 'CA 451'));
    console.log(newBoards.onBoard(new Car('black', 2, 'CA 451')));

    assert.equal(results, "accepted");

  });



})
